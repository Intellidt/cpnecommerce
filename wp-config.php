<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '8<bN_;V&2>KAN3>f.l`3s??yq7OXQ+;Ulv]lk ~X(Kv5zCeOr`gaZI|fnh{c/WJj' );
define( 'SECURE_AUTH_KEY',  '[Ep_1MQ(*|$thU&FvOwx.gT`IQucYkTA!!|s[[I7:SDaG!^lX~EZaSCErFz=$?#m' );
define( 'LOGGED_IN_KEY',    'S1lsw>;;c64/xmLqxb.{@Ube]#AZlz~n|-KhD</?.wt(M_9bTz?#qjay.3.?&>s5' );
define( 'NONCE_KEY',        '^mz6OoM*i}6`l5wi}JAA[N?jPf3I(:o&& oG&LW/Z#3dwaTUk;Mro0t4SKOeCw#q' );
define( 'AUTH_SALT',        '9jij?XbeXpBw8eAzh* a+nn2S9-(L@PiHVa+1rQOd`Z_ke<7-mz#P3i.IE!3[q!%' );
define( 'SECURE_AUTH_SALT', 'W<Od5UrVEIx}qz$M24nu*A`6lni:*NOqndCtr/a3?%P!sv/MZ5681_r)FLy)$8xT' );
define( 'LOGGED_IN_SALT',   'Lkd*2TdR3%7jTa6W|ONvt/_-e5DbQGe;e(Bl~3oxUZj9!qjP>;Z!XsqRf46T!R=*' );
define( 'NONCE_SALT',       '@|q$a/[t=@7zGFND!&zXWdL3b)bc?N=3ve~rK1Lx~S$?:=4Y=r(GH}u%R -gB(K$' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
